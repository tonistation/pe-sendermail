const fs = require('fs');
const readline = require('readline');
const express = require('express');
const app = express();
app.use(express.json()); // to support JSON-encoded bodies
app.use(express.urlencoded()); // to support URL-encoded bodies

const http = require('http').Server(app);
var bodyParser = require('body-parser')
app.use(bodyParser.json({ // to support URL-encoded bodies
    extended: true
})); // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ // to support URL-encoded bodies
    extended: true
}));

const {
    google
} = require('googleapis');

const nodemailer = require("nodemailer");



// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/gmail.readonly'];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = 'token.json';

// Load client secrets from a local file.
function startSendEmail(dataEmail, response) {
    fs.readFile('credentials.json', (err, content) => {
        if (err) return console.log('Error loading client secret file:', err);
        authorize(JSON.parse(content), dataEmail, response);
    });
}


/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, dataEmail, response) {
    const {
        client_secret,
        client_id,
        redirect_uris
    } = credentials.installed;

    const oAuth2Client = new google.auth.OAuth2(
        client_id, client_secret, redirect_uris[0]);

    // Check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, (err, token) => {
        if (err) return getNewToken(oAuth2Client, dataEmail, response);
        oAuth2Client.setCredentials(JSON.parse(token));
        //callback(oAuth2Client);
        //console.log(JSON.parse(token)) 
        sendMail(dataEmail, response);
    });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback, response) {
    const authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES,
    });

    console.log('Authorize this app by visiting this url:', authUrl);
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });
    rl.question('Enter the code from that page here: ', (code) => {
        rl.close();
        oAuth2Client.getToken(code, (err, token) => {
            if (err) return console.error('Error retrieving access token', err);
            oAuth2Client.setCredentials(token);
            // Store the token to disk for later program executions
            fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
                if (err) return console.error(err);
                console.log('Token stored to', TOKEN_PATH);
            });
            callback(oAuth2Client);
        });
    });
}

/**
 * Lists the labels in the user's account.
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
function sendMail(dataEmail, response) {
    //console.log(response);
    const smtpTransport = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: {
            user: "esconfacloud@gmail.com",
            pass: "Lima2019"
        },
        tls: {
            rejectUnauthorized: false
        }
    });


    var mailOptions = {
        from: 'esconfacloud@gmail.com',
        to: dataEmail.email,
        subject: dataEmail.subject,
        text: dataEmail.body,
        html: dataEmail.body,
    };


    smtpTransport.sendMail(mailOptions, (err, res) => {
        if (err) {
            response.json({
                success: false,
                msg: err
            });
        } else {
            //console.log(JSON.stringify(res));
            response.json({
                success: true,
                msg: res
            });
        }
    });
}

app.post('/send-email', function (req, res) { // Grab the form data and send email
    console.log(req.body.email);
    console.log(req.body.subject);
    console.log(req.body.body);

    if (req.body.email !== undefined && req.body.subject !== undefined && req.body.body !== undefined) {
        startSendEmail(req.body, res);
    } else {
        console.log("sss");
        res.json({
            error: true,
            msg: 'invalid data'
        });
    }
});

let server = app.listen(8081, function () {
    let port = server.address().port;
    console.log("Server started at http://localhost:%s", port);
});